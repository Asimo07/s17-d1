// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Aces', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitso'];
let room = ['401A', '401B', '401C'];
// Not recommeded use of array
let mixedArr = [12 , 'Asus', null, undefined, {}];

// Alternative way to write arrays
let myTasks = [
'drink HTML',
'eat Javascript',
'inhale CSS',
'bake SASS'
];
// index = n - 1 where n is the total length of array

// Reassigning array values
console.log('Array before reassignment')
console.log(myTasks);
myTasks[0] = 'clean node';
console.log('Array after reassignment');
console.log(myTasks);

// Reading from Array

console.log(grades[0]);
console.log(computerBrands[3]);
// Accessing an array element that does not exist
console.log(grades[20]);

// Getting the length of an array (.length)

console.log(computerBrands.length);

// Manipulating Arrays
// Array Methods
//  Mutator Methods - functions that "mutate" or change an array after they're created

let fruits = ['Apple', 'Orange', 'Kiwi','Dragon Fruit'];

// push() - adds an element in the end of an array and returns the array's length.
console.log('Current Aray');
console.log(fruits);
let fruitsLength = fruits.push('Mango')
console.log(fruitsLength)
console.log('Mutated array from push method')
console.log(fruits);

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method')
console.log(fruits);

//  pop() - removes the last element in an array and returns the removed element
let removeFruit = fruits.pop();
console.log(removeFruit);
console.log('Mutated array from pop method')
console.log(fruits);

// unshift() - adds one or more elements at the beginning of an array
/* Syntax:
	arrayName.unshift('elementA');
	arrayName.unshift('elementA', 'elementB')
*/

fruits.unshift('Lime', 'Banana');
console.log('Mutated array from unshift method');
console.log(fruits);

/* Shift() - removes an element at the beginning of an array and returns the removed elements.
Syntax: 
arrayName.shift();*/

let anotherFruit = fruits.shift()
console.log(anotherFruit);
console.log('Mutated array from shift method');
console.log(fruits);

/* Splice() - simultaneously removes elements from a specified index number and adds elements.
Syntax: 
arrayName.splice(startingIndex, deleteCount, elementsToBoAdded)
*/

fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method');
console.log(fruits);


/* sort() - rearranges the array elements in alphanumeric order:
Syntax: 
arrayName.sort();
*/

fruits.sort();
console.log('Mutated array from sort method');
console.log(fruits);

/* reverse () - reversed the order of array elements.
Syntax:
arrayName.reverse();
*/

fruits.reverse();
console.log('Mutated array from reverse method');
console.log(fruits);

// -----------------------------------------------------------------------------------

/* Non-mutator methods - function that do not modify or change an array after they're created 
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

/* indexOf() - returns the index number of the first matching element found in an array
Syntax:
arrayName.indexOf(searchValue);
*/

countries.indexOf('PH');

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method:' + firstIndex);

countries.indexOf('BR');
let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf method:' + invalidCountry);

/* lastIndexOf() - returns the index number of the last matching element found in an array. 
Syntax:
arrayName.lastIndexOf(searchValue);
*/

let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex);

//  slice() - portions/slices elements from an array and returns a new array
/* Syntax:
	arrayName.slice(startingIndex);
	arrayName.slice(startingIndex, lengthCount);
*/

console.log(countries);
//  Slicing off elements from a specified index to another index
let sliceArrayA = countries.slice(2);
console.log('Result from slice method');
console.log(sliceArrayA);


let sliceArrayB = countries.slice(2, 6);
console.log('Result from slice method');
console.log(sliceArrayB);

/* toString() - returns an array as a string separated by commas.
Syntax:
arrayName.toString();
*/
let stringArray = countries.toString();
console.log('Result from toString method');
console.log(stringArray);

/* concat() - combines two arrays and returns the combined result.
Sytanx:
arrayA.concat(arrayB);
*/

let tasksArrayA = ['drink html', 'eat Javascript'];
let tasksArrayB = ['inhale css', 'breathe sass'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method');
console.log(tasks);

// combinining multiple arrays
console.log('Result from concat method')
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat method')
console.log(combinedTasks);

/* join() - returns an array as a string separated by specified separator string.
Syntax: 
arrayName.join('separatorString');
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// ---------------------------------------------------------------------------

// Iteration methods - are loops designed to perform repetitive tasks on arrays
// - common practice to use the singular form of the array content for parameter names

// forEach() - similar to a for loop that iterates on each array element
/* Syntax:
	arrayName.forEach(function(indivElement)){
		statement;
	}

*/
console.log()
allTasks.forEach(function(task){
	console.log(task);
});

// Using forEach with condition statements
let filteredTasks = [];

allTasks.forEach(function(tasks){
	if(tasks.length > 10){
		filteredTasks.push(tasks)
	}
});

console.log('Result of filtered tasks: ')
console.log(filteredTasks)

// map() - iterates on each element and returns a new array with different values depending on the result of the function's operation.
/* Syntax:
	let/cons resultArray = arrayName.map(function(indivElement){
		statement/s;
	})
*/

let numbers = [1, 2, 3, 4, 5];
let numberMap = numbers.map(function(number){
	return number * number;
});

console.log('Result of map method: ')
console.log(numberMap);

/* every() - checks if all elements in an array meet the given condition
Syntax:
	let/const resultArray = arrayName.every(function(indivElement){
		return expression/condition;
	})

	similar to AND if one condition did not meet the result will be false
*/

let allValid = numbers.every(function(number){
	return (number < 3 );
});
console.log('Result of map method: ')
console.log(allValid);

//  some() - check if at least one element in the array meets the given condition 
/* Syntax:
	let/const resultArray = arrayName.some(function(indivElement){
		return expression/condition;
	})	

	similar to OR if one condition did not meet the result will be true
*/

let someValid = numbers.some(function(number){
	return (number < 2 );
});
console.log('Result of map method: ')
console.log(someValid);

/* filter() - return a new array that contains elements which meets the given condition.
Syntax:
	let/const resultArray = arrayName.filter(funtion(indivElement){
	return expression/condition;
	})

 output is the element that meets the condition	
*/

let filterValid = numbers.filter(function(number){
	return (number < 3 )
});
console.log('Result of map method: ')
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number === 0);
});
console.log('Result of filter method: ')
console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = [];

numbers.forEach(function(number){
	if(number > 3){
		filteredNumbers.push(number);
	}
})
console.log('Result of filter method: ')
console.log(filteredNumbers);

// includes() - the result of the first method is used on the second method until all "chained" methods have been resolved

//  output will be the filtered elements inside the variable inside().
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
});
console.log(filteredProducts);

// reduce() - evaluates elements from left to right and returns or reduces the array into a single value
/* Syntax:
	let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
	return expression/operation
	})
*/

// iteration      1  2  3     4  
//                |  |  |   x=10 y = 15 
//                |  |  x=6 y  |
//                | x=3 y  |   |
//				  x	 y  |  |   |
// let numbers = [1, 2, 3, 4, 5];
let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration: '+ ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	return x + y;
})
console.log('Result of reduce method: ' + reducedArray);

// Reducing string arrays
let list = ['Hello', 'Again', 'World'];
let reducedJoin = list.reduce(function(x, y){
	return x + ' ' + y;
});
console.log("Result of reduce method: " + reducedJoin);

// Multidimensional Array
// Two dimensional Array - having an array within an array

let oneDim = [];
// 1st Dim       0      1      index
// 2nd Dim     0  1   0  1     index
let twoDim = [[2, 4],[6, 8]];
// 2x2 Two Dimensional Array
console.log(twoDim[1][0]);
console.log(twoDim[0][1]);
console.log(twoDim[1][1]);

// 3x2 Two Dimensional Array
//			      0		 1       2
//			    0  1   0  1    0  1
let twoDim2 = [[2, 4],[6, 8],[10, 12]];
console.log(twoDim2[2][0]);

function myName(name){
	console.log('Hello' + name);
}